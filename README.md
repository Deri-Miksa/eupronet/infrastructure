# Docker based infrastructure 

Current docker infrastructure:

![Docker infrastructure plan](infra_3.0.png)
  
## Setup
1. `sudo apt install docker.io docker-compose -y`
2. Start docker daemon
3. Pull this repo
4. Create a proper .env file 
5. Start the stacks with `docker-compose up -d` command


## Starting sequence
1. caddy
2. mariadb
3. backend
4. frontend
5. portainer
6. watchtower
