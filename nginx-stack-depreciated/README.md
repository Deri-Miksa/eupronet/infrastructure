# letsencrypt-nginx-proxy-companion 
  
> It handles the automated creation, renewal and use of Let's Encrypt certificates for proxied Docker containers.
  
https://github.com/nginx-proxy/docker-letsencrypt-nginx-proxy-companion  
  
